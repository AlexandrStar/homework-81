const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const nanoid = require('nanoid');

const Url = require('./models/Url');

const app = express();
app.use(cors());
app.use(express.json());

const port = 8000;

app.post('/', (req, res) => {
  const unitUrl = {
    longUrl: req.body.longUrl,
    shortUrl: nanoid(7)
  };

  const url = new Url(unitUrl);

  url.save(url)
    .then(result => res.send(result.shortUrl))
    .catch(error => res.status(400).send(error))
});

app.get(`/:url`, (req, res) => {
  Url.findOne({shortUrl: req.params.url})
    .then(url=> res.status(301).redirect(url.longUrl))
    .catch(()=>res.sendStatus(500))
});

mongoose.connect('mongodb://localhost/url', {useNewUrlParser: true}).then(() => {

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });
});