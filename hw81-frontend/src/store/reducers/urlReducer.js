import {CREATE_URL_SUCCESS} from "../actions/urlAction";

const initialState = {
  url: '',
};

const urlReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_URL_SUCCESS:
      return {...state, url: action.url};
    default:
      return state;
  }
};

export default urlReducer;