import axios from '../../axios-api';

export const CREATE_URL_SUCCESS = 'CREATE_URL_SUCCESS';

export const createUrlSuccess = url => {return {type: CREATE_URL_SUCCESS, url};
};

export const postUrl = url => {
  return dispatch => {
    return axios.post('/', url).then(
      response => {
        dispatch(createUrlSuccess(response.data))
      }
    );
  };
};