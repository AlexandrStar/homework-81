import React, {Component, Fragment} from 'react';
import {Container} from "reactstrap";
import {Route, Switch} from "react-router-dom";
import ShortLink from "./containers/ShortLink/ShortLink";

class App extends Component {
  render() {
    return (
      <Fragment>
        <Container>
          <Switch>
            <Route path="/" exact component={ShortLink}/>
          </Switch>
        </Container>
      </Fragment>
    );
  }
}

export default App;
