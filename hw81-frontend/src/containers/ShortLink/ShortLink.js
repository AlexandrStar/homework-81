import React, {Component, Fragment} from 'react';
import {Button, Col, Form, FormGroup, Input} from "reactstrap";
import {connect} from "react-redux";
import {postUrl} from "../../store/actions/urlAction";
import {apiURL} from "../../constans";

class ShortLink extends Component {
  state = {
    longUrl: '',
  };

  submitFormHandler = event => {
    event.preventDefault();
    const longUrl = this.state;
    this.props.postUrl(longUrl);
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };
  render() {
    return (
      <Fragment>
        <h1 style={{textAlign: 'center', marginTop: '50px'}}>Shorten your link!</h1>
        <Form onSubmit={this.submitFormHandler}>
          <FormGroup row>
            <Col sm={{offset:1, size: 10}}>
              <Input
                type="text" required
                name="longUrl" id="longUrl"
                placeholder="Enter URL here"
                value={this.state.longUrl}
                onChange={this.inputChangeHandler}
              />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Col sm={{offset: 5, size: 2}}>
              <Button type="submit" color="primary">Shorten!</Button>
            </Col>
          </FormGroup>
        </Form>
        {this.props.url.url === '' ? null :
          <div style={{textAlign: 'center', marginTop: '50px'}}>
            <h4> Your link now looks like this:</h4>
            <a
              href={apiURL + '/' + this.props.url.url}
              target="_blank"
              rel="noopener noreferrer"
            >
              {apiURL + '/' + this.props.url.url}
              </a>
          </div>
        }
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    url: state.url,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    postUrl: url => dispatch(postUrl(url))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ShortLink);